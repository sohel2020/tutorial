import requests
import json
from requests.auth import HTTPBasicAuth
import pika
from threading import Thread
import httplib, sys
from Queue import Queue


# HTTP_HOST = "http://teststresstest.yo.com:1568/api/"
HTTP_HOST = "http://teststresstest.yo.com:1568/api/"

API_USERS = "users"

HTTP_USER = "admin"
HTTP_PASS = "admin"
API_PERMISSIONS = "permissions"
VHOST_NAME = "text"

start = 10001
end=start+1000

concurrent = 10
total = concurrent * 1000

def doWork():
    i=1
    while True:
        url = q.get()
        status, url = getStatus(url, i)
        doSomethingWithResult(status, url)
        i=i+1
        q.task_done()

def getStatus(ourl, i):
    try:
        password = "u"+str(ourl)
        queue_name = "u"+str(ourl)
        
        params = {"password": password, "tags": "YO!User"}
        header = {'Accept': 'application/json',
                          'Content-type': 'Application/json'}

        r = requests.put(HTTP_HOST+API_USERS+"/"+queue_name,
                                  data=json.dumps(params),
                                  headers=header,
                                  auth=HTTPBasicAuth(HTTP_USER, HTTP_PASS))

       
        if r.status_code == 200 or r.status_code == 204:
            rg = requests.get(HTTP_HOST+API_USERS+'/'+queue_name+'/'+API_PERMISSIONS, auth=HTTPBasicAuth(HTTP_USER,HTTP_PASS))
           
            if rg.status_code == 200 or rg.status_code == 204:
                json_value = rg.json()       

                params = {"configure": ".*","write": ".*","read": ".*"}

                rs = requests.put(HTTP_HOST+API_PERMISSIONS+"/"+VHOST_NAME+"/"+queue_name,
                                 data=json.dumps(params), headers=header, auth=HTTPBasicAuth(HTTP_USER, HTTP_PASS))

               




        credentials = pika.PlainCredentials(HTTP_USER, HTTP_PASS)
        connection = pika.BlockingConnection(pika.ConnectionParameters(
                    host="teststresstest.yo.com",
                    port=7856,
                    # ssl=True,
                    credentials=credentials,
                    virtual_host=VHOST_NAME,
                    socket_timeout=10000000)
                )
        channel = connection.channel()
        channel.queue_declare(queue=queue_name, durable=True)


        return queue_name, ourl
    except Exception, e:       
        
        return 'Error '+str(e), ourl

def doSomethingWithResult(status, url):
    print status


q = Queue(total)
for i in range(concurrent):
    t = Thread(target=doWork)
    print t
    t.daemon = True
    t.start()
try:
    for i in range(start, end):        
        url = i
        q.put(url)
    q.join()
    print 'Complete'
except KeyboardInterrupt:
    sys.exit(1)


print "Complete--------------------"
